      (function () {
        'use strict';

        window.addEventListener('load', function () {
          var list = document.querySelector('ul');
          var form = document.querySelector('#addForm');
          var description = document.querySelector('#description');
          description.focus();
          var quantity = document.querySelector('#quantity');
          
          var logoutButton = document.querySelector('#logoutButton');
          var loginButton = document.querySelector('#loginButton');
          var emailField = document.querySelector('#email');
          var passwordField = document.querySelector('#password');

          var body = document.querySelector('body');
          
          function addItem( record ) {
            var listItem = document.createElement('li');
            var listText = document.createElement('span');
            var listBtn = document.createElement('button');
            listBtn.setAttribute( "class", "btn btn-danger showloggedin");
            listItem.setAttribute( "class", "list-group-item d-flex justify-content-between align-items-center");
            listItem.appendChild(listText);
            listText.textContent = record.Quantity + " " + record.Description;
            listItem.appendChild(listBtn);
            listBtn.textContent = window.location.href.includes( '/fr/' ) ? 'Effacer' : 'Delete';
            list.appendChild(listItem);
            listBtn.onclick = function(e) {
              list.removeChild(listItem);

              var request = new XMLHttpRequest();
              request.open("DELETE", "../shoppinglist.php?ID=" + record.ID );
              request.send();
              
              description.focus();
            };
          }

          function handleSubmit(event) {
            if (this.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
              return;
            }
            this.classList.remove('was-validated');
            
            var record = { "Description": description.value, "Quantity": quantity.value };
            addItem( record );
            
            var formData = new FormData();
            formData.append("Description", description.value );
            formData.append("Quantity", quantity.value );

            var request = new XMLHttpRequest();
            request.addEventListener("load", function() {
              record.ID = this.response.ID;
            });
            request.responseType = "json";
            request.open("POST", "../shoppinglist.php");
            request.send(formData);

            description.value = '';
            quantity.value = '1';
            description.focus();
            event.preventDefault();
            event.stopPropagation();
            form.classList.remove('was-validated');
          }
          form.addEventListener('submit', handleSubmit );
          
          
          function reqListener () {
            this.response.forEach( function( record ){
              console.log( record );
              addItem( record );
            });
          }

          var oReq = new XMLHttpRequest();
          oReq.addEventListener("load", reqListener);
          oReq.responseType = "json";
          oReq.open("GET", "../shoppinglist.php");
          oReq.send();

          function authListener () {
            console.log( "auth: " + this.response );
            if ( this.response ) {
              body.setAttribute( 'class', 'container loggedin');    
            }
          }
          
          function handleLogin() {
            console.log( "login" );
            var formData = new FormData();
            formData.append("inputEmail", emailField.value );
            formData.append("inputPassword", passwordField.value );

            var request = new XMLHttpRequest();
            request.addEventListener("load", authListener );
            request.responseType = "json";
            request.open("POST", "../auth.php");
            request.send(formData);
          }
          
          loginButton.addEventListener( 'click', handleLogin );
          
          function handleLogout() {
            console.log( "logoutButton" );

            var request = new XMLHttpRequest();
            request.open("DELETE", "../auth.php");
            request.send();
            
            body.setAttribute( 'class', 'container notloggedin');
          }
          
          logoutButton.addEventListener( 'click', handleLogout );

          var aReq = new XMLHttpRequest();
          aReq.addEventListener("load", authListener);
          aReq.responseType = "json";
          aReq.open("GET", "../auth.php");
          aReq.send();
        }, false);
      }());    
