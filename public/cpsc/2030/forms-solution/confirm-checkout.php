<!doctype html>
<html lang="en">
<?php
  setlocale( LC_MONETARY, "en_CA" );
  
  $qty1 = $_REQUEST['qty1'];
  $qty2 = $_REQUEST['qty2'];
  $qty3 = $_REQUEST['qty3'];
  $firstName = $_REQUEST['firstName'];
  $lastName = $_REQUEST['lastName'];
  $email = $_REQUEST['email'];
  $address = $_REQUEST['address'];
  $address2 = $_REQUEST['address2'];
  $country = $_REQUEST['country'];
  $province = $_REQUEST['province'];
  $postalCode = $_REQUEST['postalCode'];
  if ( isset($_REQUEST['giftWrap'])) $giftWrap = $_REQUEST['giftWrap'];
  $deliveryMethod = $_REQUEST['deliveryMethod'];
  $ccname = $_REQUEST['ccname'];
  $ccnumber = $_REQUEST['ccnumber'];
  $ccexpiration = $_REQUEST['ccexpiration'];
  $cccvv = $_REQUEST['cccvv'];
  
  $gst = 0;
  $pst = 0;
  $hst = 0;
  $gift = 0;
  $delivery = 0;
  $subtotal = 0;
  $grandTotal = 0;
  
  $subtotal += $qty1*12 + $qty2*8 + $qty3*5;
  
  if ( isset( $giftWrap) ) $gift = 5;
  
  $deliveryCosts = [
    "mail" => 5,
    "courier" => 50,
    "pickup" => 0,
  ];
  
  $delivery = $deliveryCosts[$deliveryMethod];

  $taxes = [
    "Alberta" => [ "gst" => 0.05, "pst" => 0.00, "hst" => 0.00 ],
    "British Columbia" => [ "gst" => 0.05, "pst" => 0.07, "hst" => 0.00 ],
    "Manitoba" => [ "gst" => 0.05, "pst" => 0.08, "hst" => 0.00 ],
    "New Brunswick" => [ "gst" => 0.00, "pst" => 0.00, "hst" => 0.15 ],
    "Newfoundland and Labrador" => [ "gst" => 0.00, "pst" => 0.00, "hst" => 0.15 ],
    "Northwest Territories" => [ "gst" => 0.05, "pst" => 0.00, "hst" => 0.00 ],
    "Nova Scotia" => [ "gst" => 0.00, "pst" => 0.00, "hst" => 0.15 ],
    "Nunavut" => [ "gst" => 0.05, "pst" => 0.00, "hst" => 0.00 ],
    "Ontario" => [ "gst" => 0.00, "pst" => 0.00, "hst" => 0.13 ],
    "Prince Edward Island" => [ "gst" => 0.00, "pst" => 0.00, "hst" => 0.15 ],
    "Quebec" => [ "gst" => 0.06, "pst" => 0.09975, "hst" => 0.00 ],
    "Saskatchewan" => [ "gst" => 0.05, "pst" => 0.06, "hst" => 0.00 ],
    "Yukon" => [ "gst" => 0.05, "pst" => 0.00, "hst" => 0.00 ]
  ];

  $gst = ($subtotal + $gift + $delivery) * $taxes[$province]["gst"];
  $pst = ($subtotal + $gift + $delivery) * $taxes[$province]["pst"];
  $hst = ($subtotal + $gift + $delivery) * $taxes[$province]["hst"];
  
  $grandTotal = $subtotal + $gift + $delivery + $gst + $pst + $hst;
  
?>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Checkout confrimation · Bootstrap</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" src="bootstrap-solid.svg" alt="" width="72" height="72">
    <h2>Checkout confirmation</h2>
    <p class="lead">Below is an example of a checkout confirmation page.</p>
  </div>

  <div class="row">
    <div class="col-md-6 order-md-2 mb-4">
      <h4 class="mb-1">Your cart</h4>
      <div class="row mb-2">
        <div class="col-5">
          <h6 class="my-0">Product name</h6>
          <small class="text-muted">Brief description</small>
        </div>
        <div class="col-4">
          <?php print $qty1 ?> @ $12
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $qty1*12 ) ?>
        </div>
      </div>
      <div class="row mb-2">
        <div class="col-5">
          <h6 class="my-0">Second product</h6>
          <small class="text-muted">Brief description</small>
        </div>
        <div class="col-4">
          <?php print $qty2 ?> @ $8</label>
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $qty2*8 ) ?>
        </div>
      </div>
      <div class="row mb-2">
        <div class="col-5">
          <h6 class="my-0">Third item</h6>
          <small class="text-muted">Brief description</small>
        </div>
        <div class="col-4">
          <?php print $qty3 ?> @ $5</label>
        </div>
        <div class="col-3 text-right border-bottom border-dark">
          <?php print money_format( "%n", $qty3*5 ) ?>
        </div>
      </div>
      
      <div class="row mb-2">
        <div class="col-9 text-right">
          Subtotal
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $subtotal ) ?>
        </div>
      </div>
      
      <?php if ( isset( $giftWrap )) { ?>
      <div class="row mb-2">
        <div class="col-9 text-right">
          Gift wrap
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", 5 ) ?>
        </div>
      </div>
      <?php } ?> 
      
      <div class="row mb-2">
        <div class="col-9 text-right">
          Delivery (<?php print $deliveryMethod; ?>)
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $delivery ) ?>
        </div>
      </div>

      <?php if ( $pst > 0 ) { ?>
      <div class="row mb-2">
        <div class="col-9 text-right">
          PST
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $pst ) ?>
        </div>
      </div>
      <?php } ?> 
      
      <?php if ( $gst > 0 ) { ?>
      <div class="row mb-2">
        <div class="col-9 text-right">
          GST
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $gst ) ?>
        </div>
      </div>
      <?php } ?> 

      <?php if ( $hst > 0 ) { ?>
      <div class="row mb-2">
        <div class="col-9 text-right">
          HST
        </div>
        <div class="col-3 text-right">
          <?php print money_format( "%n", $hst ) ?>
        </div>
      </div>
      <?php } ?> 
      
      <div class="row mb-2">
        <div class="col-9 text-right">
          Grand Total
        </div>
        <div class="col-3 text-right border-top border-dark">
          <?php print money_format( "%n", $grandTotal ) ?>
        </div>
      </div>
      
    </div>
    <div class="col-md-6 order-md-1">
      <h4 class="mb-1">Billing address</h4>
      <div class="mb-3">
        <?php
          echo "$firstName $lastName<br>";
          if ( !empty( $email )) echo "$email<br>";
          echo "$address<br>";
          if ( !empty( $address2 )) echo "$address2<br>";
          echo "$province $postalCode<br>"
        ?>
      </div>  
      <h4 class="mb-1">Payment</h4>
      <div class="mb-3">
        <?php
          echo "$ccname<br>";
          echo "$ccnumber<br>";
          echo "Exp. $ccexpiration CVV $cccvv<br>"
        ?>
      </div>  
    </div>
  </div>
  
  <?php if ( $grandTotal > 750 ) { ?>
  <div class="alert alert-danger text-center" role="alert">
    Credit card declined!
  </div>
  <?php } ?>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2017-2019 Company Name</p>
    <ul class="list-inline">
      <li class="list-inline-item"><a href="#">Privacy</a></li>
      <li class="list-inline-item"><a href="#">Terms</a></li>
      <li class="list-inline-item"><a href="#">Support</a></li>
    </ul>
  </footer>
</div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
